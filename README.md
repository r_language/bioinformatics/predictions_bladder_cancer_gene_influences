In this R file we created multiple classifiers with bladder cancer data (gene influences) : 

- Multiclasspairs

- Random Forest

- Pamr

- Random Forest SRC

We also used those classifiers to perform predictions, visualize top k genes, and we compared those classifiers (accuracy, kappa, etc). You can have a look at the HTML output as well. 

Author : Marion Estoup 

Mail : marion_110@hotmail.fr

Date : August 2023